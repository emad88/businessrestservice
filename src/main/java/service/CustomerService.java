package service;

import model.Contract;
import model.Customer;
import model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
@Component
public class CustomerService {

    @Autowired
    private ContractService contractService;
    private List<Customer> customers = new ArrayList<>();
    private List<Integer> customersId = new ArrayList<>();


    // Add operation has O(N) complexity
    public Response addCustomer(Customer customer) {
        Response response = new Response();
        if (!customersId.contains(customer.getId())) {
            customersId.add(customer.getId());
            customers.add(customer);
            response.setResponse("Customer added");
            return response;
        }
        response.setResponse("Customer with this id already exists");
        return response;
    }

    // O(N) complexity
    public Customer getCustomer(int customerId) {
        List<Contract> contracts = contractService.getContractsByCustomerId(customerId);

        for (Customer customer : customers) {
            if (customer.getId() == customerId) {
                customer.setContracts(contracts);
                return customer;
            }
        }
        return null;
    }

    public List<Integer> getCustomersId() {
        return customersId;
    }
}
