package service;

import model.Contract;
import model.ContractTypes;
import model.Response;
import model.Revenue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */

@Component
public class ContractService {

    @Autowired
    private CustomerService customerService;
    private List<Integer> contractsId = new ArrayList<>();
    private List<Contract> contracts = new ArrayList<>();

    //O(N) complexity
    public Response addContract(Contract contract) {
        List<Integer> customersId = customerService.getCustomersId();
        Response response = new Response();
        if (customersId.contains(contract.getCustomerId())) {
            contractsId.add(contract.getId());
            contracts.add(contract);
            response.setResponse("Contract is added");
            return response;
        }

        response.setResponse("Either contract with the same customerId exists or the customerId does not exist");
        return response;
    }
    //O(N) complexity
    public Revenue totalRevenuesForSpecificType(ContractTypes contractType) {
        double sum = 0;
        Revenue revenue = new Revenue();
        for (Contract contract : contracts) {
            if (contract.getType() == contractType)
                sum += contract.getRevenue();
        }
        revenue.setRevenue(sum);
        return revenue;
    }

    public Revenue totalRevenuesForSpecificCustomer(int id) {
        double sum = 0;
        Revenue revenue = new Revenue();
        for (Contract contract : contracts) {
            if (contract.getCustomerId() == id) {
                sum += contract.getRevenue();
            }
        }
        revenue.setRevenue(sum);
        return revenue;
    }

    public List<Contract> getContractsByCustomerId(int customerId) {
        List<Contract> contractsForACustomer = new ArrayList<>();
        for (Contract contract : contracts) {
            if (contract.getCustomerId() == customerId) {
                contractsForACustomer.add(contract);
            }
        }
        return contractsForACustomer;
    }

    private double convertToOneDecimal(double number) {
        DecimalFormat oneDForm = new DecimalFormat("#.#");
        return Double.valueOf(oneDForm.format(number));
    }


}
