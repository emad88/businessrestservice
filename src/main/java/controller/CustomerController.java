package controller;

import model.Customer;
import model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.CustomerService;

import javax.validation.Valid;
import java.util.concurrent.Callable;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
@RestController
@RequestMapping("/customerservice/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/add",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json")
    private Callable<Response> addCustomer(@Valid @RequestBody final Customer customer) {
        Callable<Response> asyncResult = new Callable<Response>() {
            @Override
            public Response call() throws Exception {
                Response response = customerService.addCustomer(customer);
                return response;
            }
        };

        return asyncResult;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    private Callable<Customer> getCustomer(@PathVariable("id") int customerId) {

        Callable<Customer> asyncResult = new Callable<Customer>() {
            @Override
            public Customer call() throws Exception {
                Customer customer = customerService.getCustomer(customerId);
                return customer;
            }
        };

        return asyncResult;
    }

}
