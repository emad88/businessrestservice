package com.business;

import configuration.SpringConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
public class BusinessApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringConfiguration.class);
        builder.headless(false);
        ConfigurableApplicationContext applicationContext = builder.run(args);
    }
}
