# README #
## What framework and IDE has been used? ##
The application is written with Maven, Spring boot, in Intellij IDE. 


## What is JSON format? ##
### ADD Customer ###
{
  "id"       : 1235,
  "fullName" : "Emad Nikkhouy",
  "email"    : "emad.nikkhouy@gmail.com"
}

### Add Contract ###
 {
  "id"        : 1227,
  "customerId": 1235,
  "startDate" : "2012-01-06",
  "type"      : "PERMANENT",   
  "revenue"   : 23.23
}


## Contract types? ##
* PERMANENT
* TEMPORARY


## RESULT ##
### ADD Customer ###
* (http://localhost:8080/customerservice/customer/add)
* {"response": "Customer Emad Nikkhouy added"}
* Adding Again the same ID:   
* {"response": "Customer with id 1235 already exists"}

### Add Contract ###
* (http://localhost:8080/customerservice/contract/add)
* input:
* {
  "id"        : 1227,
  "customerId": 1235,
   "startDate" : "2012-01-06",
   "type" :     "PERMANENT",   
  "revenue" : 23.23 }
* output: 
* {"response": "Contract 1227 added"}

* Adding the same contract again:
* {"response": "Contract with the same CustomerId exsists"}

* Adding a contract with wrong customerId:
* {"response": "customerId does not exist"}

### Calculate Revenue ### 
* NOTE: I woulnd't use same path for getting the total revenue based on Type and CustomerId, however I did it since the assignment asked for it, so if in the URL user inserts integer program will try to find customerId, else it will try to find contract type

* URLs:
* Using contract type:
* http://localhost:8080/customerservice/contract/permanent
* Using Customer ID:
* http://localhost:8080/customerservice/contract/1235 

* Sample output: 
{"revenue": 69.69}

### Get Customer ### 
* URL: http://localhost:8080/customerservice/customer/1235

* {
    "fullName": "Emad Nikkhouy",
    "email": "emad.nikkhouy@gmail.com",
    "id": 1235,
    "contracts": [
        {
            "id": 1227,
            "customerId": 1235,
            "startDate": "2012-01-06",
            "type": "PERMANENT",
            "revenue": 23.23
        },
        {
            "id": 1224,
            "customerId": 1235,
            "startDate": "2012-01-06",
            "type": "PERMANENT",
            "revenue": 23.23
        },
        {
            "id": 1226,
            "customerId": 1235,
            "startDate": "2012-01-06",
            "type": "TEMPORARY",
            "revenue": 23.23
        }
    ]
}