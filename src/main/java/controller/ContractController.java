package controller;

import com.sun.org.apache.regexp.internal.RE;
import model.Contract;
import model.ContractTypes;
import model.Response;
import model.Revenue;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.ContractService;

import javax.validation.Valid;
import java.util.concurrent.Callable;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
@RestController
@RequestMapping("/customerservice/contract/")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @RequestMapping(value = "/add",
            method = RequestMethod.PUT,
            consumes = "application/json",
            produces = "application/json")
    private Callable<Response> addContract(@Valid @RequestBody final Contract contract) {

        Callable<Response> asyncResult = new Callable<Response>() {
            @Override
            public Response call() throws Exception {
                Response response =contractService.addContract(contract);
                return response;
                }
            };

        return asyncResult;
    }

    @RequestMapping(value = "/{param}", method = RequestMethod.GET, produces = "application/json")
    private Callable<Revenue> getTotalRevenues(@PathVariable("param") final String type) {

        Callable<Revenue> asyncResult = new Callable<Revenue>() {
            @Override
            public Revenue call() throws Exception {
                Revenue revenue;
                if (StringUtils.isNotEmpty(type) && StringUtils.isNumeric(type)) {
                    revenue = contractService.totalRevenuesForSpecificCustomer(Integer.valueOf(type));
                } else {
                    ContractTypes contractType = ContractTypes.valueOf(type.toUpperCase());
                    revenue = contractService.totalRevenuesForSpecificType(contractType);
                }
                return revenue;
            }
        };

        return asyncResult;
    }


}
