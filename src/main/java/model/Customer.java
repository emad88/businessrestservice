package model;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */

@Component
public class Customer {

    private String fullName;
    private String email;
    private int id;
    private List<Contract> contracts;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }
}
