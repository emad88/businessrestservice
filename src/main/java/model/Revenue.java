package model;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
public class Revenue {

    private double revenue;

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
}
