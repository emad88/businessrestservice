package model;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
public enum ContractTypes {
    PERMANENT,
    TEMPORARY;
}
