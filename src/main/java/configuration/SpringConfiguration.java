package configuration;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */

@SpringBootApplication
@EnableAsync
@ComponentScan("controller, service, model")
public class SpringConfiguration {
    // some beans in future
}
