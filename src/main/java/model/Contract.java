package model;

/**
 * Created by emadnikkhouy on 23/04/2017.
 */
public class Contract {

    private int id;
    private int customerId;
    private String startDate;
    private ContractTypes type;
    private double revenue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public ContractTypes getType() {
        return type;
    }

    public void setType(ContractTypes type) {
        this.type = type;
    }

    public double getRevenue() {
        return revenue;
    }

    public void setRevenue(double revenue) {
        this.revenue = revenue;
    }
}
